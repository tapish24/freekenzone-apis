const express = require('express');
const app = express();
const {
    check,
    validationResult
} = require('express-validator/check');
const morgan = require('morgan');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
require('dotenv').config();
const http = require('http');
const jwt = require('jsonwebtoken');
const mongoDBErrors = require('mongoose-mongodb-errors');
const cors = require('cors'); 
const gcm = require('node-gcm');
const session = require('express-session');
const flash = require('connect-flash');
app.use(cors());

// const User = require("./models/user");

const api = require('./api/routes/index');

app.use(morgan('dev'));
app.use('/uploads', express.static('uploads'));
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(bodyParser.json());

// Enable CORS
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept, Authorization"
    );
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
});

// Get the Access Token
// TODO: Validate the JWT token
//
app.use((req, res, next) => {

    if (req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {

        const token = req.headers.authorization.split(' ')[1];

        if (token) {
            return jwt.verify(token, 'secret', (err, userData) => {
                if (err) {
                    return res.status(401).json({
                        success: false,
                        message: "User is not authenticated",
                    });
                }
                req.user = {
                    id: userData._id,
                    email: userData.email,
                    role: userData.role,
                    token: token,
                    exp: userData.exp
                }
                return next();
            });
        }
        return res.unauthorized();
    }
    next();
});

app.get('/', (req, res) => {res.send('Freeken-zone API')});
app.use('/api/', api);
app.use((req, res, next) => {
    const error = new Error('Not found');
    error.status = 404;
    next(error);
})

app.use((error, req, res, next) => {
    res.status(error.status || 500)
    res.json({
        error: error.status,
        message: error.message
        // stack: error.stack
    });
});
/*          const gcmKey = '1061332147717'; // Your gcm key in quotes
const deviceToken = 'ednZOyoC-ZQ:APA91bFix-qqqdcNUbI5YUeHeVe27fgIqXM7lWmilhfQCGerQ7lXyMb-j7rEumel8Ee5H1fHiOLELKbMYW9D7lOb7ld0HE6P6OgWoYaxWtLY8y4PXfE4iuhLXcBb4JBlfBl_2dtsUDj0'; // Receiver device token
const sender = new gcm.Sender('AAAA9xxTXgU:APA91bG62-K1FmioZuDd9N-85h9bMBDziLSoXediC6iIsA_9kIL3y_acL-eHkRJOrhbc8k0l0IQOLdtWT5JnB8YRa8djB8yqs6wyp2CxPUw5ss8teFuRcDIN-Njs5rWqzL0dtM6uq2aW');

var message = new gcm.Message();

message.addData({
    title: 'Follow Request',
    body: 'This is push notification',
    otherProperty: true,
});

sender.send(message, {
    registrationIds: ['ednZOyoC-ZQ:APA91bFix-qqqdcNUbI5YUeHeVe27fgIqXM7lWmilhfQCGerQ7lXyMb-j7rEumel8Ee5H1fHiOLELKbMYW9D7lOb7ld0HE6P6OgWoYaxWtLY8y4PXfE4iuhLXcBb4JBlfBl_2dtsUDj0']
}, (err) => {
    if (err) {
        console.error(err);
    } else {
        console.log('Sent');
    }
});*/

mongoose.connect(process.env.MONGO_URL,  { useNewUrlParser: true }).then(() => console.log('Mongo Connected'));
mongoose.plugin(mongoDBErrors);
app.listen(process.env.PORT, () => console.log('Server started on: ' + process.env.PORT ));

/*mongoose.connect("mongodb://Freekenzone:freekenzone001@ds123603.mlab.com:23603/heroku_bmgl2pcv");

mongoose.plugin(mongoDBErrors);*/
mongoose.Promise = global.Promise;
module.exports = app;