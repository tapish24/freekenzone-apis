const mongoose = require('mongoose');


const GeoSchema =  new mongoose.Schema({
  type: {
    type: String,
    default: 'Point'
  },
  coordinates: {
    type: [Number],   // <longitude>, <latitude> 
    index: '2dsphere'
  }
});

const SettingSaveSchema = mongoose.Schema({
   
    preferanceCategory: [{
        type: String,
        default: ''
    }],

    preferanceLocation: GeoSchema

}, {
    timestamps: true
});

module.exports = mongoose.model('SettingSave', SettingSaveSchema);
