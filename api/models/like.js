const mongoose = require('mongoose');

const likeSchema = mongoose.Schema({
    userId: {
       type: String,
        default: ''
    },
     eventId: {
       type: String,
        default: ''
    },
    isdraft: {
       type: Boolean,
       //default: ''
    },
  },  {
timestamps : true
});

module.exports = mongoose.model('Like', likeSchema);
