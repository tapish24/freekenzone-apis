const mongoose = require('mongoose');

const eventCategorySchema = mongoose.Schema({
    // _id: mongoose.Schema.Types.ObjectId,
    categoryName: {
        type: String
    },
    Status: {
        type: String,
        default: false
    },
    isdraft: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('EventCategory', eventCategorySchema);