const mongoose = require('mongoose');

const InterestSchema = mongoose.Schema({
    event: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event'
    },
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    isInterest: {
        type: Boolean,
        default: 'false'
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('Interest', InterestSchema);