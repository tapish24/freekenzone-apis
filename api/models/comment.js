const mongoose = require('mongoose');

const CommentSchema = mongoose.Schema({
     event: { 
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event'
    },
        user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
  },
      comment: { 
        type: String, 
        default: ''
    },
    isdraft: {
       type: Boolean,
       //default: ''
    },
}, {
timestamps: true
});

module.exports = mongoose.model('Comment', CommentSchema);

