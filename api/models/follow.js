const mongoose = require('mongoose');

const FollowerSchema = mongoose.Schema({
    senderId: {
        type: String,
        default: true
    },
    recieverId: {
        type: String,
        default: false
    },

    status: {
        type: Number,
        default: 0
    }
}, {
    timestamps: true
});

module.exports = mongoose.model('Follow', FollowerSchema);
