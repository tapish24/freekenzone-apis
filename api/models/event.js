const mongoose = require('mongoose');
var timestamps = require('mongoose-timestamp');


const CommentSchema = mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    comment: {
        type: String
    },
    userName: {
        type: String
    },
    userImage: {
        type: String
    },
    isdraft: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true
});

//Advise schema
const AdviseSchema = mongoose.Schema({

    adviseStarts: {
        type: String
    },

    adviseEnds: {
        type: String
    },
    duration: {
        type: String
    },
    adviseStatus: {
        type: String
    },
    adviseCity: {
        type: String
    },
    adviseRegion: {
        type: String
    },
    adviseNation: {
        type: String
    },
    adviseContinent: {
        type: String
    },
    adviseWorld: {
        type: String
    },
}, {
    timestamps: true
});

// Notification Schema
const NotificationSchema = mongoose.Schema({

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
    event: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'Event'
    },
    notificationType: {
        type: String
    }
}, {
    timestamps: true
});

//Medal Schema
const MedalSchema = mongoose.Schema({

    medalName: {
        type: String
    },
    price: {
        type: Number
    }
}, {
    timestamps: true
});

// Geo-location
const GeoSchema = new mongoose.Schema({
    type: {
        type: String,
        default: 'Point'
    },
    coordinates: {
        type: [Number], // <longitude>, <latitude> 
        index: '2dsphere'
    }
});

const eventSchema = mongoose.Schema({
    // _id: mongoose.Schema.Types.ObjectId,
    title: {
        type: String,
        default: ''
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
   
    oraganizerName: {
        type: String,
        default: ''
    },
    eventImage: {
        type: String,
        default: ''
    },
    description: {
        type: String,
        default: ''
    },
    startDate: {
        type: Date,
        default: Date.now

    },
    endDate: {
        type: Date,
        default: Date.now
    },
    startTime: {
        type: String,
        default: ''
    },
    endTime: {
        type: String,
        default: ''
    },
    address: {
        type: String,
        default: ''
    },
    location: GeoSchema,
    city: {
        type: String,
        default: ''
    },
    eventType: {
        type: [String],
        default: ''
    },
    selectedCategoryName: {
        type: [String]
    },
    eventCategory: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'EventCategory'
        }]
    },
     eventCategoryId: {
        type: [String]
    },
    privacy: {
        type: String,
        default: ''
    },
    count: {
        type: String,
        default: ''
    },
    duration: {
        type: Number,
        default: 40
    },
    like: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }]
    },
    likeTest: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }]
    },

    followers: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }]
    },
    following: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }]
    },
    interest: {
        type: [{
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User'
        }]
    },

    isDraft: {
        type: Boolean,
        default: false
    },
    notification: [NotificationSchema],
    advise: [AdviseSchema],
    medal: [MedalSchema],
    comment: [CommentSchema]
}, {
    timestamps: true
});
eventSchema.index({ 'location': '2dsphere' })
// eventSchema.plugin(timestamps);
module.exports = mongoose.model('Event', eventSchema);