const mongoose = require('mongoose');

const eventTypeSchema = mongoose.Schema({
    eventType: {
        type: String
    },
    isdraft: {
        type: Boolean,
        default: false
    },
}, {
    timestamps: true
});

module.exports = mongoose.model('EventType', eventTypeSchema);