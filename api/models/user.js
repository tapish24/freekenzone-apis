const mongoose = require('mongoose');
const bcrypt = require('bcrypt-nodejs');



const FollowerSchema = mongoose.Schema({
    isPending: {
        type: Boolean,
        default: true
    },
    isContactVisible: {
        type: Boolean,
        default: false
    },

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});

const FollowingSchema = mongoose.Schema({
    isPending: {
        type: Boolean,
        default: true
    },

    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    }
}, {
    timestamps: true
});


const NotificationSchema = mongoose.Schema({
     requestedUserId: {
        type: String
    },
     acceptUserId: {
        type: String
    },
      user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'User'
    },
       body: {
        type: String
    },

    isSeen:{
        type: Boolean,
        default: 0
    },
     notificationStatus: {
        type: Number,
        default: 0
    },

}, {
    timestamps: true
});


const GeoSchema =  new mongoose.Schema({
  type: {
    type: String,
    default: 'Point'
  },
  coordinates: {
    type: [Number],   // <longitude>, <latitude> 
    index: '2dsphere'
  }
});
const SettingSaveSchema = new mongoose.Schema({
    preferanceCategory: {
        type: [String],
        default: ''
    },

    preferanceLocation: GeoSchema,

    preferanceAddress: {
        type: String,
        default: ''
    }

}, {
    timestamps: true
});




const UserSchema = mongoose.Schema({
    //   _id: mongoose.Schema.Types.ObjectId,
    email: {
        type: String,
        unique: true,
        match: /[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/
    },

    password: {
        type: String
    },

    image: {
        type: String,
        default: ''
    },

    firstName: {
        type: String,
        default: ''
    },
    lastName: {
        type: String,
        default: ''
    },
    dateOfBirth: {
        type: String,
        default: ''

    },
    gender: {
        type: String,
        default: ''
    },

  // isFollowing: {
  //       type: Number,
  //       default: 0
  //   },
    //  isUnFollowing: {
    //     type: Boolean,
    //     default: false
    // },

    companyName: {
        type: String,
        default: ''
    },

    phoneNumber: {
        type: String,
        default: ''
    },

    state: {
        type: String,
        default: ''
    },

    city: {
        type: String,
        default: ''
    },

    address: {
        type: String,
        default: ''
    },
    deviceType: {
        type: String,
        default: ''
    },
    deviceId: {
        type: String,
        default: ''
    },

    pinCode: {
        type: String,
        default: ''
    },

    facebookId: {
        type: String,
        default: ''
    },
    skypeId: {
        type: String,
        default: ''
    },

    instagramId: {
        type: String,
        default: ''
    },

   
    saveSetting:[SettingSaveSchema],

    followers: [FollowerSchema],

    following: [FollowingSchema],
    notification: [NotificationSchema],
    
    role: {
        type: String,
        default: 'user'
    },
},

 {
    timestamps: true
});
UserSchema.index({ 'location': '2dsphere' }),

UserSchema.pre('save', function(next) {
    var user = this;
    if (this.isModified('password') || this.isNew) {
        bcrypt.genSalt(10, function(err, salt) {
            if (err) {
                return next(err);
            }
            // console.log('user =' +user)
            bcrypt.hash(user.password, salt, null, function(err, hash) {
                if (err) {
                    return next(err);
                }
                user.password = hash;
                // console.log('hash =' +hash)
                next();
            });
        });
    } else {
        return next();
    }
});

UserSchema.methods.comparePassword = function(passw, pwd, cb) {

    bcrypt.compare(passw, this.password, function(err, isMatch) {
        //console.log('err = ' + err)

        if (err) {
            return err;
        }
        console.log('isMatch = ' + isMatch)
        cb(null, isMatch);
    });
};


module.exports = mongoose.model('User', UserSchema);