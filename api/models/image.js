const mongoose = require('mongoose');

const imageUploadSchema = mongoose.Schema({
   userId: {
       type: String,
        default: ''
    },
    eventId: {
       type: String,
        default: ''
    },
     eventImage: {
       type: String,
        default: ''
    },
    isdraft: {
       type: Boolean,
       //default: ''
    },
  },  {
timestamps : true
});

module.exports = mongoose.model('Image', imageUploadSchema);

