const jwt = require('jsonwebtoken');

module.exports.userAuthenticator = (req, res, next) => {
    if (!req.user) {
        return res.status(401).json({
            message: 'User is not authenticated'
        });
    } else if (req.user && (req.user.exp < (new Date().getTime() / 1000))) {
        return res.status(401).json({
            message: 'User session expire'
        });
    } else {
        next()
    }
};

/**
 * Authentication Middleware for Admins
 */
module.exports.adminAuthenticator = (req, res, next) => {
    const admins = process.env.ADMINS.split(',')

    if (admins.indexOf(req.user.email) !== -1) {
        req.user.isSuperAdmin = true
        next()
    } else {
        return res.status(401).json({
            message: 'User not authorizede'
        });    
    }
}

/*module.exports.userAuthenticator = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        const decoded = jwt.verify(token, 'secret');
        req.user = decoded;
        next();
    } catch (error) {
        return res.status(401).json({
            message: 'User is not authenticated'
        });
    }
};*/