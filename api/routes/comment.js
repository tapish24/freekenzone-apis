const express = require("express");
const router = express.Router();
const Comment = require('../controllers/comment');


exports.addComment = (req, res, next) => {
    const event = req.params.eventId;
    const commentObject = {
        'comment': req.body.comment,
        'user': req.user.id,
        userImage: req.body.userImage,
        userName: req.body.userName
    }
    Comment.createComment(commentObject, event).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.fetctSingleEventComment = (req, res, next) => {
    const eventId = req.params.eventId
    const userId = req.user.id
    Comment.fetchSingleComment(eventId, userId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find commentId'))

};


module.exports.fetchCommentList = (req, res, next) => {
    const eventId = req.params.eventId
    Comment.fetchAllComment(eventId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find commentId'))

};


module.exports.commentDelete = (req, res, next) => {

    const commentId = req.params.commentId;
    const eventId = req.params.eventId

    Comment.CommentDelete(eventId, commentId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find commentId'))
};

module.exports.commentUpdate = (req, res, next) => {
    const commentId = req.params.commentId;
    Comment.CommentUpdate(commentId, req.body).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find commentId'))
};


/*module.exports.commentDelete = (req, res, next)  => {
    console.log(req.params.commentId)
   const _commentId = req.params._commentId 
    Comment.CommentDelete(_commentId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find commentId'))

};*/

module.exports.viewCommentList = (req, res, next) => {
    const userId = req.params.userId
    Comment.viewCommentsList(userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find commentId'))

};