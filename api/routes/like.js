const express = require("express");
const router = express.Router();
const Like = require('../controllers/like');


exports.like = (req, res, next) => {
    const userId = req.params.userId;
    const eventId = req.params.eventId
    Like.eventLike(userId,eventId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};