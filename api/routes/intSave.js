const express = require("express");
const router = express.Router();
const Interest = require('../controllers/intSave');




exports.eventSave = (req, res, next) => {

    // const event = req.params.eventId; 

    const interestObject = {
        'event': req.body.eventId,
        'user': req.body.userId
    }
    Interest.createintrestSave(interestObject).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

exports.fetchInterestSave = (req, res, next) => {
    // console.log(req.params.eventTypeId)
    const userId = req.params.userId
    Interest.fetchInterest(userId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};

module.exports.interestRemove = (req, res, next) => {
    // console.log(req.params.eventId)
    const eventId = req.params.eventId
    console.log(req.params.eventId)
    Interest.removeInterestedEvent(eventId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))
};