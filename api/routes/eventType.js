const express = require("express");
const router = express.Router();
const EventType = require('../controllers/eventType');


module.exports.addEventtype = (req, res, next) => {
    const eventObject = {
        'eventType': req.body.eventType,
    }
    EventType.createEventType(eventObject).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.viewEventType = (req, res, next) => {
    const eventTypeId = req.params.eventTypeId
    EventType.viewEventType(eventTypeId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};

module.exports.eventTypeupdate = (req, res, next) => {
    const eventTypeId = req.params.eventTypeId;
    EventType.EventTypeUpdate(eventTypeId, req.body).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find eventTypeId'))

};


module.exports.EventTypeDelete = (req, res, next) => {
    const eventTypeId = req.params.eventTypeId
    EventType.EventTypeDelete(eventTypeId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};

module.exports.allEventType = (req, res, next) => {
    const eventTypeId = req.params.eventTypeId
    EventType.viewallEventType(eventTypeId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};