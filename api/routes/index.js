const express = require("express");
const router = express.Router();
const multer = require('multer');
const UserController = require('../controllers/user');
const {userAuthenticator, adminAuthenticator} = require('../middleware/check-auth');
const EventController = require('../controllers/event');
const EventCategoryController = require('../controllers/eventCategory');
const Event1 = require('../controllers/event');
const Auth = require('./auth');
const Event = require('./event');
const EventCategory = require('./eventCategory');
const EventType = require('./eventType');
const Comment = require('./comment');
const Interest = require('./intSave');
const Like = require('./like');
const Follow = require('./follow');
const ImageController = require('../controllers/image');
const {
    check,
    validationResult
} = require('express-validator/check');

const storage = multer.diskStorage({
    destination: function(req, file, cb) {
        cb(null, './uploads/');
    },
    filename: function(req, file, cb) {
        cb(null, new Date().toISOString() + file.originalname);
    }
});

const fileFilter = (req, file, cb) => {
    // reject a file
    if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
        cb(null, true);
    } else {
        cb(null, false);
    }
};

const upload = multer({
    storage: storage,
    limits: {
        fileSize: 1024 * 1024 * 5
    },
    fileFilter: fileFilter
});


router.put("/interestSave/:eventId", [userAuthenticator], Event.eventSave);
router.get("/fetchSaveEvent", [userAuthenticator], Event.fetchInterestSave);
router.put("/removeSave/:eventId", [userAuthenticator], Event.removeEvent);

// Auth Route
router.get("/viewOtherUserProfile/:userId", [userAuthenticator], Auth.otherUserProfile);
router.post("/Signup", UserController.userSignup);
router.post("/Login", Auth.login);
router.get("/viewProfile/:userId", [userAuthenticator], Auth.viewProfile);
router.put("/profileUpdate/:userId", [userAuthenticator], Auth.profileUpdate);
//router.put("/settingSave/:userId", [userAuthenticator], Auth.settingUpdate);

router.put("/settingSave/:userId", [userAuthenticator], Auth.settingSave);
router.get("/viewSaveSetting/:settingId", Auth.viewSaveSetting);



router.get("/viewAllUser", [userAuthenticator], Auth.allUser);
router.put("/forgotPassword", Auth.emailSent);

// Socail Login Route

router.post("/fbLogin", Auth.fblogin);
router.post("/instaLogin", Auth.instalogin);

// Follow/Unfollow Route

router.put("/follow", [userAuthenticator], Auth.follower);
router.put("/requestNotification", [userAuthenticator], Auth.followNotification);
router.put("/accept", [userAuthenticator], Auth.acceptRequest);
router.put("/reject", [userAuthenticator], Auth.rejectRequest);
router.put("/UnFollow", [userAuthenticator], Auth.UnFollowUser);
router.put("/contactIsVisible/:requestedUserId", [userAuthenticator], Auth.contactVisible);
router.get("/followingList/:userId", [userAuthenticator], Auth.listOfFollowing);
router.get("/followersList/:userId", [userAuthenticator], Auth.listOfFollowers);
router.get("/notificationList", [userAuthenticator], Auth.listOfNotification);
router.get("/isSeenList", [userAuthenticator], Auth.listOfIsSeen);



// Event Route
router.get("/searchEvent", Event.searchAllEvent);

router.get("/search", Event.searchEvent);


router.post("/createEvent", [userAuthenticator], Event.addEvent);
router.post("/createManyEvent", Event.addManyEvent);
router.get("/eventNotificationList", [userAuthenticator], Event.fetchNotification);
router.get("/viewEventdetails/:eventId", [userAuthenticator], Event.viewEventdetais);
router.put('/eventDetailsUpdate/:eventId', [userAuthenticator], Event.eventdetailsupdate);
router.get("/myEvent/:userId", [userAuthenticator], Event.userEventlist);
router.put("/event/notification", [userAuthenticator], Event.generatorNotification);

router.post("/imageUpload", ImageController.eventImage);
router.post("/import", Event1.eventCsv);

router.delete("/deleteEvent/:eventId", [userAuthenticator], Event.EventDelete);
router.get("/allEventList", [userAuthenticator], Event.allEventlist);

// Event-Live-Past-Draft Route
router.get("/isDraftEvent/:userId", [userAuthenticator], Event.fetchDraftEvent);
router.get("/liveEvents", [userAuthenticator], Event.fetchLiveEvent);
router.get("/pastEvent", [userAuthenticator], Event.fetchPastEvent);

// Event-Like-Dislike Route
router.put("/eventLike/:eventId", [userAuthenticator], Event.eventLike);
router.put("/eventdisLike/:eventId", [userAuthenticator], Event.eventdisLike);
router.post("/like/:userId/:eventId", Like.like);

// Event-Category Route
router.post("/createEventCategory", [userAuthenticator], EventCategory.createEventCategory);
router.get("/viewEventCategory/:eventCategoryId", [userAuthenticator], EventCategory.viewEventCategory);
router.put('/eventCategoryUpdate/:eventCategoryId', [userAuthenticator], EventCategory.eventcategoryupdate);
router.put('/updateStatus/:eventCategoryId', [userAuthenticator], EventCategory.updateStatus);
router.delete("/deleteEventCategory/:eventCategoryId", [userAuthenticator], EventCategory.CategoryDelete);
router.get("/viewEventCategoryList", [userAuthenticator], EventCategory.allEventCategory);

// Event-Type Route
router.post("/createEventType", [userAuthenticator], EventType.addEventtype);
router.get("/viewEventType/:eventTypeId", [userAuthenticator], EventType.viewEventType);
router.put('/eventTypeUpdate/:eventTypeId', [userAuthenticator], EventType.eventTypeupdate);
router.delete('/eventTypeDelete/:eventTypeId', [userAuthenticator], EventType.EventTypeDelete);
router.get("/viewEventTypeList", [userAuthenticator], EventType.allEventType);

// Event-Comment-Route
router.put("/createComment/:eventId", [userAuthenticator], Comment.addComment);
router.get("/event/comment/:eventId", [userAuthenticator], Comment.fetctSingleEventComment);
router.patch('/commentUpdate/:commentId', [userAuthenticator], Comment.commentUpdate);
router.delete('/event/:eventId/comment/:commentId', [userAuthenticator], Comment.commentDelete);
router.get("/commentList/:userId", [userAuthenticator], Comment.viewCommentList);
router.get("/event/commentList/:eventId", [userAuthenticator], Comment.fetchCommentList);

module.exports = router;