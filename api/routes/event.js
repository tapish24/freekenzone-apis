const mongoose = require('mongoose');

const express = require("express");
const router = express.Router();
const Event = require('../controllers/event');
const {filterObject} = require('../utils/stringUtils');
const { check, validationResult } = require('express-validator/check');

const errorCheck = require('../middleware/errorHandler');

module.exports.addEvent = (req, res, next) => {

    const postData = filterObject(req.body, ['title', 'oraganizerName', 'userId', 'eventImage', 'userImage', 'count', 'description', 'startDate', 'endDate', 'startTime', 'endTime', 'address',  'endTime', 'address', 'eventType', 'eventCategory', 'noOfTicket', 'location', 'like', 'disLike', 'followers', 'following', 'city', 'selectedCategoryName', 'privacy' ])
    const newData =  Object.assign({eventCategoryId: postData.eventCategory}, postData);
    Event.createEvent(newData).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.addManyEvent = (req, res, next) => {

    const postData = filterObject(req.body, ['title', 'oraganizerName', 'userId', 'eventImage', 'userImage', 'count', 'description', 'startDate', 'endDate', 'startTime', 'endTime', 'address',  'endTime', 'address', 'eventType', 'eventCategory', 'noOfTicket', 'location', 'like', 'disLike', 'followers', 'following', 'city', 'selectedCategoryName', 'privacy' ])
    const newData =  Object.assign({eventCategoryId: postData.eventCategory}, postData);
    Event.createManyEvent(newData).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.viewEventdetais = (req, res, next) => {
    const eventId = req.params.eventId;
    const userId = req.user.id;

    Event.viewEvent(eventId, userId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};

module.exports.eventdetailsupdate = (req, res, next) => {
    const eventId = req.params.eventId;
    Event.EventUpdate(eventId, req.body).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

/*module.exports.viewEventList = (req, res, next)  => {
    console.log(req.params.userId)
   const userId = req.params.userId 
    Event.viewEventList(userId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};*/

module.exports.EventDelete = (req, res, next) => {
    const eventId = req.params.eventId
    Event.EventDelete(eventId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))
};

  module.exports.userEventlist = (req, res, next) => {
    const userId = req.params.userId;
     Event.viewEventList(userId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))
};

module.exports.generatorNotification = (req, res, next) => {
    const userId = req.body.userId;
    const eventId = req.body.eventId;
    const notificationType = req.body.notificationType;
    Event.notification(userId, eventId, notificationType).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};

module.exports.fetchNotification = (req, res, next) => {
    const userId = req.user.id;
    Event.fetchAllNotification(userId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};

module.exports.allEventlist = (req, res, next) => {
    const eventId = req.params.eventId;
    const userId = req.user.id;

    Event.viewallEventList(eventId, userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find userId'))
};

module.exports.fetchDraftEvent = (req, res, next) => {
    const userId = req.params.userId;

    Event.isDraftEvent(userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find userId'))
};

module.exports.fetchLiveEvent = (req, res, next) => {
    const userId = req.user.id;
    Event.liveEvent(userId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};

module.exports.fetchPastEvent = (req, res, next) => {
    const userId = req.user.id;
    Event.pastEvent(userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find userId'))
};

module.exports.eventLike = (req, res, next) => {
    const userId = req.user.id;
    const eventId = req.params.eventId;

    Event.eventLikes(userId, eventId).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};

module.exports.eventdisLike = (req, res, next) => {
    const userId = req.user.id;
    const eventId = req.params.eventId;
    Event.eventDisLikes(userId, eventId).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};


module.exports.searchAllEvent = (req, res, next) => {

    let startDate
    let endDate

    if (typeof(req.query.startDate) !== 'undefined' && req.query.startDate !== '') {

        const date = new Date(req.query.startDate);
        date.setDate(date.getDate());
        startDate = date.toISOString();

        const date1 = new Date(req.query.startDate);
        date1.setDate(date1.getDate() + 1);
        endDate = date1.toISOString();
    } else {
        startDate = null;
        endDate = null;
    }

    let category = []

    if (typeof(req.query.eventCategory) !== 'undefined' && req.query.eventCategory !== '') {
        const eventCategory = req.query.eventCategory;
        // category = eventCategory.split(',');
        eventCategory.split(',').map((m) => {
            category.push(m)
        })
    } else {
        category = [];
    }
    // console.log(category)

    let lat
    let lng
    if (typeof(req.query.lat) !== 'undefined' && typeof(req.query.lng) !== 'undefined' && req.query.lat !== '' && req.query.lng !== '') {
         lat =  req.query.lat;
         lng = req.query.lng;
      
    } else {

        lat = 0;
        lng = 0;
    }

    Event.filterEvent(startDate, endDate, lat, lng, category).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};


module.exports.searchEventMidlleware = [
  // username must be an email
    check('check').exists(),

  check('check').not().isEmpty(),

  errorCheck.requestValidator
];



module.exports.searchEvent = (req, res, next) => {

    let check = req.query.check;
    Event.search(check).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};



module.exports.eventSave = (req, res, next) => {
    const userId = req.user.id;
    const eventId = req.params.eventId;

    Event.interestSave(userId, eventId).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};

module.exports.removeEvent = (req, res, next) => {
    const userId = req.user.id;
    const eventId = req.params.eventId;
    Event.removeInterestSave(userId, eventId).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};

exports.fetchInterestSave = (req, res, next) => {

    const userId = req.user.id;
    Event.fetchInterest(userId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};