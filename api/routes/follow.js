const express = require("express");
const router = express.Router();
const Follow = require('../controllers/follow');



module.exports.followRequest = (req, res, next) => {
    const followRequestObject = {
        'senderId': req.body.req.user.id,
        'recieverId':req.body.recieverId,
        'status':req.body.status
    }
    Follow.userFollow(followRequestObject).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.acceptRequest = (req, res, next) => {

    const acceptUserId = req.body.acceptUserId
    const requestedUserId = req.body.requestedUserId;
    const isPendingStatus = false;


    User.requestAccept(acceptUserId, requestedUserId, isPendingStatus).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};


module.exports.rejectRequest = (req, res, next) => {

    const rejectUserId = req.body.rejectUserId
    const requestedUserId = req.body.requestedUserId;

    User.requestReject(rejectUserId, requestedUserId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};


module.exports.UnFollowUser = (req, res, next) => {

    const acceptUserId = req.body.acceptUserId;
    const rejectUserId = req.body.rejectUserId;

    User.userUnFollow(rejectUserId,acceptUserId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.contactVisible = (req, res, next) => {

    const user = req.user.id
    const requestedUserId = req.params.requestedUserId;

    User.contactVisibility(user, requestedUserId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};

module.exports.listOfFollowing = (req, res, next) => {

    const userId = req.user.id
    User.followingList(userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find userId'))
};