const express = require("express");
const router = express.Router();
const User = require('../controllers/user');

const {filterObject} = require('../utils/stringUtils');


module.exports.settingSave = (req, res, next) => {
    const userId = req.params.userId;
    const preferanceCategory = req.body.preferanceCategory;
    const preferanceLocation = req.body.preferanceLocation;
    const preferanceAddress = req.body.preferanceAddress;

    User.settingSaveFun(preferanceCategory,preferanceLocation,preferanceAddress,userId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};

// module.exports.settingSave = (req, res, next) => {
//     const userId = req.params.userId;
//     const postData = filterObject(req.body, ['preferanceCategory',  'preferanceLocation' ])
//     User.settingSaveFun(postData,userId).then((result) => {
//         res.json(result)
//     }).catch((error) => next(error))

// };

module.exports.viewSaveSetting = (req, res, next) => {
    // console.log('user = ' + req.user)
    const settingId = req.params.settingId;
    User.viewSaveSettingFun(settingId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find settingId'))

};

module.exports.login = (req, res, next) => {

    const email = req.body.email;
    const password = req.body.password;
    const deviceType = req.body.deviceType;
    const deviceId = req.body.deviceId;
    const role = req.body.role || 'user'

    User.userLogin(email, password, role,deviceType,deviceId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to create the requested resource'))

};

module.exports.viewProfile = (req, res, next) => {
    // console.log('user = ' + req.user)
    const userId = req.params.userId;
    User.viewProfileFunc(userId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};

module.exports.otherUserProfile = (req, res, next) => {
    console.log('user = ' + req.user)
    const userId = req.params.userId;
       const loginUser = req.user.id;
    User.otherUser(userId,loginUser).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};

module.exports.profileUpdate = (req, res, next) => {
    const userId = req.params.userId;
    User.ProfileUpdate(userId, req.body).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};

module.exports.settingUpdate = (req, res, next) => {
    const userId = req.params.userId;
    User.setttingUpdateFunc(userId, req.body).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};



exports.fblogin = (req, res, next) => {
    const email = req.body.email;
    const firstName = req.body.firstName;
    const facebookId = req.body.facebookId

    User.Fblogin(email, firstName, facebookId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to create the requested resource'))

};

exports.instalogin = (req, res, next) => {
    const firstName = req.body.firstName;
    const instagramId = req.body.instagramId

    User.instaLogin(firstName, instagramId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.allUser = (req, res, next) => {

    const userId = req.params.userId
    User.viewallUser(userId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};


module.exports.emailSent = (req, res, next) => {

    const email = req.body.email
    User.testEmail(email).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};



module.exports.follower = (req, res, next) => {
    const notification = req.body.notification;
    const following = req.body.following;
    const notificationStatus = 1;
    const deviceId = req.body.deviceId;
    const followingUserId = {
        'user': following
    };
    const userId = req.user.id;
     const recieverUserId = {
        'user': notification
    };
     // const notificationStatus = 1
    // const requestNotification = 1

    User.userFollow(followingUserId, userId,notificationStatus,deviceId).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};

module.exports.acceptRequest = (req, res, next) => {

    const acceptUserId = req.body.acceptUserId
    const requestedUserId = req.body.requestedUserId;
    const isPendingStatus = false;
    const notificationStatus = 0;
    


    User.requestAccept(acceptUserId, requestedUserId, notificationStatus, isPendingStatus).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};


module.exports.rejectRequest = (req, res, next) => {

    const rejectUserId = req.body.rejectUserId
    const requestedUserId = req.body.requestedUserId;

    User.requestReject(rejectUserId, requestedUserId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};


module.exports.UnFollowUser = (req, res, next) => {

    const acceptUserId = req.body.acceptUserId;
    const requestedtUserId = req.body.requestedtUserId;

    User.userUnFollow(requestedtUserId,acceptUserId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.contactVisible = (req, res, next) => {

    const user = req.user.id
    const requestedUserId = req.params.requestedUserId;

    User.contactVisibility(user, requestedUserId).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};

module.exports.followNotification = (req, res, next) => {
    const loginUserId = req.body.loginUserId;
    const recieverUserId = req.body.recieverUserId;
    const notificationStatus = req.body.notificationStatus;
    User.notification(loginUserId, recieverUserId, notificationStatus).then((result) => {
        res.json(result)
    }).catch((error) => next(error))
};

module.exports.listOfFollowing = (req, res, next) => {

    const userId = req.params.userId;
    User.followingList(userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find userId'))
};


module.exports.listOfFollowers = (req, res, next) => {

    const userId = req.params.userId;
    User.followersList(userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find userId'))
};

module.exports.listOfNotification = (req, res, next) => {

    const userId = req.user.id;
    User.notificationList(userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find userId'))
};


module.exports.listOfIsSeen = (req, res, next) => {

    const userId = req.user.id;
    User.notification(userId).then((result) => {
        res.json(result)
    }).catch((error) => next('Failed to find userId'))
};

