const express = require("express");
const router = express.Router();
const EventCategory = require('../controllers/eventCategory');


module.exports.createEventCategory = (req, res, next) => {
    const eventObject = {
     'categoryName': req.body.categoryName,
     'Status': req.body.Status,
    }
    EventCategory.addEventCategory(eventObject).then((result) => {
        res.json(result)
    }).catch((error) => next(error))

};

module.exports.viewEventCategory = (req, res, next)  => {
   const eventCategoryId = req.params.eventCategoryId 
    EventCategory.viewEventCategory(eventCategoryId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};

module.exports.eventcategoryupdate = (req, res, next)  => {
    const eventCategoryId = req.params.eventCategoryId;
    EventCategory.categoryUpdate( eventCategoryId, req.body).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};

module.exports.updateStatus = (req, res, next)  => {
    const eventCategoryId = req.params.eventCategoryId;
    EventCategory.categoryStatusUpdate( eventCategoryId, req.body).then((result) => {
        res.json(result)
    }).catch((error) => next(error + 'Failed to find userId'))

};


module.exports.CategoryDelete = (req, res, next)  => {
   const eventcategoryId = req.params.eventCategoryId 
    EventCategory.EventCategoryDelete(eventcategoryId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find eventCategoryId'))

};

module.exports.allEventCategory = (req, res, next)  => {

   const eventCategoryId = req.params.eventCategoryId 
    EventCategory.viewallCategory(eventCategoryId).then((result) => {
        res.json(result)

    }).catch((error) => next('Failed to find userId'))

};