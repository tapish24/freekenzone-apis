const mongoose = require("mongoose");
const Interest = require("../models/intSave");



module.exports.createintrestSave = (interestObject) => {
    return new Promise((resolve, reject) => {
        const newInterest = new Interest(interestObject);
        newInterest.save().then((interest) => {
            const responseData = {
                'success': true,
                'message': 'Successful Event save.',
                'data': interest
            }
            resolve(responseData)
        }).catch((error) => reject('Requested resource not found'))
    });
}; 


 module.exports.fetchInterest = (userId) => {
    return new Promise((resolve, reject) => {
      Interest.find({ user:userId }
            ).populate('event').then((result) => {
                if(result === null) {
                    const responseData = {
                      'success': false,
                      'message': 'userId not found.',
                    }
                    resolve(responseData)
                } else{
                  const responseData = {
                    'success': true, 
                    'message': 'All save Events fetch successfully.', 
                    'data': result
                  }
                  resolve(responseData)
              }
           }).catch((error) => reject('eventId not found'))
        })
      };


module.exports.removeInterestedEvent = (eventId) => {
    return new Promise((resolve, reject) => {
        Interest.findOneAndDelete({ _id:eventId}).then((result) => {
            if (result === null) {
                const responseData = {
                    'success': false,
                    'message': 'EventId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': false,
                    'message': ' Intrested Event remove Successfully.'
                    //user: result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('Failed to find userId'))
    })
};


/*module.exports.createintrestSave = (eventSave) => {
    return new Promise((resolve, reject) => {
        const newInterest = new Interest(eventSave).save().then(InterestInfo => {
          const responseData = {
            'success': true, 
            'message': 'Successful save Event.', 
                        'eventId': intSaveInfo._id,
                          'data':intSaveInfo}
                  resolve(responseData)

              }).catch((error) => reject('Fail user registeration'))

});
  };*/
