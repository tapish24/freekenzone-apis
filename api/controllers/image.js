const mongoose = require("mongoose");
const Image = require("../models/image");
const multer = require('multer');
const formidable = require('formidable');
const uuid = require('uuid');
const fs = require('fs');


const storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, './uploads/');
  },
  filename: function(req, file, cb) {
    cb(null, new Date().toISOString() + file.originalname);
  }
});

const fileFilter = (req, file, cb) => {
  // reject a file
  if (file.mimetype === 'image/jpeg' || file.mimetype === 'image/png') {
    cb(null, true);
  } else {
    cb(null, false);
  }
};

const upload = multer({
  storage: storage,
  limits: {
    fileSize: 1024 * 1024 * 5
  },
  fileFilter: fileFilter
});

/*exports.eventImage = (req, res, next) => {
  console.log(req.body)
  const image = new Image({
  _id: new mongoose.Types.ObjectId(),
    userId: req.body.userId,
    eventId: req.body.eventId,
    eventimage:req.body.eventimage
  });
  image
    .save()
    .then(result => {
      console.log(result);
      res.status(201).json({
        message: "Image Upload successfully",
        createdProduct: {
          name: result.name,
          price: result.price,
          _id: result._id,
          request: {
            type: "GET",
            url: "http://localhost:4000/products/" + result._id
          }
        }
      });
    })
    .catch(err => {
      console.log(err);
      res.status(500).json({
        error: err
      });
    });
};  */


exports.eventImage = (req, res, next) => {
    var hostname = req.headers.host; 

  var form = new formidable.IncomingForm();
  form.parse(req, function (err, fields, files) {
    var oldpath = files.images.path;
    var temppath = files.images.name.split(".");
    var imagename = uuid.v1() + "." + temppath[temppath.length-1];
    var imageurl =  hostname+"/uploads/"+imagename;
    var newpath = './uploads/'+ imagename;
    fs.rename(oldpath, newpath, function (err) {
      if (err){
        res.json({
          result: "false",
          data: {},
          numberOfImages: 0,
          message: "No images to upload",
          error: err 
        });
      }else{
        res.json({
          result: "true",
          data: {},
          numberOfImages: 1,
          message: "Image Upload Successfully",
          path: newpath, 
          url: imageurl
        });
      }
    });
  });
};