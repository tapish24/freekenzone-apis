const mongoose = require("mongoose");
const Event = require("../models/event");
const Notification = require("../models/notification");
const User = require("../models/user");
const csvToJson = require('convert-csv-to-json');
const  csv1      = require('csv-express');
const csv = require("fast-csv");
const fs = require('fs');
const csvfile =  './test1.csv';
const stream = fs.createReadStream(csvfile);



module.exports.eventCsv = (req, res, next) => {
    const Event  = []
    
const csvStream = csv()
        .on("data", function(data){
            console.log(data)
         
/*         const Event = new Event({
              title: data[3],
              userId: data[3],
              oraganizerName: data[3],
              description: data[3],
              startDate:data[3], 
              endDate:data[3],
              endTime:data[3],
              location:data[3],
              eventType:data[3],
              eventCategory:data[3],
              privacy:data[3],
              eventImage:data[3],


         });
         
          item.save(function(error){
            console.log(item);
              if(error){
                   throw error;
              }
          }); */
    }).on("end", function(){
          console.log(" End of file import");
    });
  
    stream.pipe(csvStream);
    res.json({success : "Data imported successfully.", status : 200});
     
  }



module.exports.createEvent = (addEvent) => {
    return new Promise((resolve, reject) => {
        const newEvent = new Event(addEvent).save().then(async (eventInfo) => {
            if (eventInfo) {
                if (eventInfo) {
                    let categoriesName = []
                    await Event.findOne({
                        _id: eventInfo._id
                    }).populate('eventCategory').
                    then(async (r) => {
                        r.eventCategory.map((c) => {
                            categoriesName.push(c.categoryName)
                        });
                        r.selectedCategoryName = categoriesName
                        r.save().then((c) => {
                            const responseData = {
                                'success': true,
                                'message': 'Successful created Event.',
                                'eventId': eventInfo._id,
                                'data': c
                            }
                            resolve(responseData)
                        }).
                        catch((error) => reject(error + ' Not found'))
                    })
                }
            } else {
                reject('Fail user registeration');
            }
        }).catch((error) => reject(error))

    });
};

module.exports.createManyEvent = (addEvent) => {
    return new Promise((resolve, reject) => {
        
 
           let fileInputName = './test.csv'; 
            let fileOutputName = './uploads/'+'myOutputFile.json';
 
    csvToJson.generateJsonFileFromCsv(fileInputName,fileOutputName);
           console.log(fileInputName)
           console.log(fileOutputName)
     /*   const newEvent = new Event(addEvent).save().then(async (eventInfo) => {
            if (eventInfo) {
                if (eventInfo) {
                    let categoriesName = []
                    await Event.findOne({
                        _id: eventInfo._id
                    }).populate('eventCategory').
                    then(async (r) => {
                        r.eventCategory.map((c) => {
                            categoriesName.push(c.categoryName)
                        });
                        r.selectedCategoryName = categoriesName
                        r.save().then((c) => {
                            const responseData = {
                                'success': true,
                                'message': 'Successful created Event.',
                                'eventId': eventInfo._id,
                                'data': c
                            }
                            resolve(responseData)
                        }).
                        catch((error) => reject(error + ' Not found'))
                    })
                }
            } else {
                reject('Fail user registeration');
            }
        }).catch((error) => reject(error))*/

    });
};

module.exports.viewEvent = (eventId, userId) => {
    return new Promise((resolve, reject) => {
        Event.findOne({
                _id: eventId
            })
            .then((event) => {
                if (event == null) {
                    const responseData = {
                        'success': false,
                        'message': 'userId not found.',
                    }
                    resolve(responseData)
                } else {

                    let isLike
                    const userFound = event.like.filter(id => id.toString() === userId);

                    if (userFound.length > 0) {
                        isLike = true
                    } else {
                        isLike = false
                    }
                    let isInterested
                    const userInterestFound = event.interest.filter(id => id.toString() === userId);

                    if (userInterestFound.length > 0) {
                        isInterested = true
                    } else {
                        isInterested = false
                    }

                    resolve({
                        'success': true,
                        'message': 'Event data fetch successfully.',
                        'data': {
                            "title": event.title,
                            "userId": event.userId,
                            "oraganizerName": event.oraganizerName,
                            "eventImage": event.eventImage,
                            "userImage": event.userImage,
                            "description": event.description,
                            "startDate": event.startDate,
                            "endDate": event.endDate,
                            "startTime": event.startTime,
                            "endTime": event.endTime,
                            "address": event.address,
                            "location": event.location,
                            "city": event.city,
                            "eventType": event.eventType,
                            "eventCategory": event.eventCategory,
                            "noOfTicket": event.noOfTicket,
                            "privacy": event.privacy,
                            "count": event.count,
                            "like": event.like,
                            "likeTest": event.likeTest,
                            "duration": event.duration,
                            "followers": event.followers,
                            "following": event.following,
                            "isDraft": event.isDraft,
                            "isLike": isLike,
                            "isInterested": isInterested,
                            "_id": event._id,
                            "comment": event.comment
                        }
                    })
                }
            }).catch((error) => reject('userId not found'))
    })
};


module.exports.EventUpdate = (eventId, brifData) => {
    return new Promise((resolve, reject) => {
        Event.findOneAndUpdate({
            _id: eventId
        }, {
            '$set': brifData
        }, {
            'new': true
        }).then(async (result) => {
            if (result === null) {
                const responseData = {
                    'success': false,
                    'message': 'eventId not found.',
                }
                resolve(responseData)
            } else {
                if (result) {
                    let categoriesName = []
                    await Event.findOne({
                        _id: result._id
                    }).populate('eventCategory').
                    then(async (r) => {
                        // console.log(r)
                        r.eventCategory.map((c) => {
                            categoriesName.push(c.categoryName)
                        });
                        r.selectedCategoryName = categoriesName
                        r.save().then((c) => {
                            const responseData = {
                                'success': true,
                                'message': 'Event Update successfully.',
                                'eventId': result._id,
                                'data': c
                            }
                            resolve(responseData)
                        }).
                        catch((error) => reject(error + ' Not found'))
                    })
                }


            }

        }).catch((error) => reject(error))

    })
};

module.exports.viewEventList = (userId) => {
    console.log(userId)
    return new Promise((resolve, reject) => {
        //console.log(userId)
        Event.find({
            userId: userId
        }).then((result) => {
            console.log(result)
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'userId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'My Event data fetch successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('userId not found'))
    })
};




module.exports.EventDelete = (eventId) => {
    return new Promise((resolve, reject) => {
        Event.findOneAndDelete({
            _id: eventId
        }).then((result) => {
            console.log(result)
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'EventId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': ' Event deleted Successfully.'
                    //user: result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('Failed to find userId'))
    })
};


module.exports.viewallEventList = (eventId, userId) => {
    return new Promise((resolve, reject) => {
        Event.find({}).sort({
                _id: -1
            }).populate('userId')
            .then((result) => {
                if (result.length === 0) {
                    const responseData = {
                        'success': false,
                        'message': 'userId not found.',
                    }
                    resolve(responseData)
                } else {
                    let isLike
                    const modifyEventResult = result.map((event) => {
                        const userFound = event.like.filter(eLike => eLike.toString() === userId);
                        if (userFound.length > 0) {
                            isLike = true
                        } else {
                            isLike = false
                        }
                        const userInterestFound = event.interest.filter(interest => interest.toString() === userId);
                        if (userInterestFound.length > 0) {
                            isInterested = true
                        } else {
                            isInterested = false
                        }

                        return {
                            "title": event.title,
                            "userId": event.userId,
                            "user":event.user,
                            "oraganizerName": event.oraganizerName,
                            "eventImage": event.eventImage,
                            "description": event.description,
                            "startDate": event.startDate,
                            "endDate": event.endDate,
                            "startTime": event.startTime,
                            "endTime": event.endTime,
                            "address": event.address,
                            "location": event.location,
                            "city": event.city,
                            "eventType": event.eventType,
                            "eventCategory": event.eventCategory,
                            "noOfTicket": event.noOfTicket,
                            "privacy": event.privacy,
                            "count": event.count,
                            "like": event.like,
                            "likeTest": event.likeTest,
                            "duration": event.duration,
                            "followers": event.followers,
                            "following": event.following,
                            "isDraft": event.isDraft,
                            "isLike": isLike,
                            "isInterested": isInterested,
                            "_id": event._id,
                            "comment": event.comment
                        }
                    })
                    resolve({
                        'success': true,
                        'message': 'All Event data fetch successfully.',
                        'data': modifyEventResult,
                        "userImage":result.userId
                    })
                }
            }).catch((error) => reject('userId not found'))
    })
};

module.exports.isDraftEvent = (userId, isDraft) => {
    return new Promise((resolve, reject) => {
        Event.find({
                "$and": [{
                    "userId": userId
                }, {
                    "isDraft": true
                }]
            })
            .populate('user').then((result) => {
                console.log(result)
                if (result == null) {
                    const responseData = {
                        'success': false,
                        'message': 'userId not found.',
                    }
                    resolve(responseData)
                } else {
                    const responseData = {
                        'success': true,
                        'message': 'Draft data fetch successfully.',
                        'data': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject('eventId not found'))
    })
};


module.exports.liveEvent = (userId) => {
    return new Promise((resolve, reject) => {
        const date = new Date()
        date.setDate(date.getDate() - 1);
        Event.find({
                "$and": [{
                    "userId": userId
                }, {
                    "startDate": {
                        '$gte': date.toISOString()
                    }
                }]
            })
            .then((result) => {
                // console.log(result)
                if (result.length === 0) {
                    const responseData = {
                        'success': false,
                        'message': 'startDate not found.',
                    }
                    resolve(responseData)
                } else {
                    const responseData = {
                        'success': true,
                        'message': 'Live Event data fetch successfully.',
                        'data': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject(error))
    })
};

module.exports.pastEvent = (userId) => {
    return new Promise((resolve, reject) => {
        const date = new Date()
        date.setDate(date.getDate() - 1);
        Event.find({
            "$and": [{
                "userId": userId
            }, {
                startDate: {
                    '$lte': date.toISOString()
                }
            }]
        }).populate('user').then((result) => {
            // console.log(result)
            if (result.length === 0) {
                const responseData = {
                    'success': false,
                    'message': 'endDate not found.',
                    'data': result
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'Past Event data fetch successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('eventId not found'))
    })
};


module.exports.eventLikes = (userId, eventId) => {
    return new Promise((resolve, reject) => {
        Event.findOneAndUpdate({
            _id: eventId
        }, {
            '$addToSet': {
                like: userId
            }
        }, {
            'new': true
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'eventId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'Event like successfully.',
                    'data': {
                        'likeCount': result.like.length,
                        'eventId': eventId,
                        'userId': userId
                    }
                }
                resolve(responseData)
            }

        }).catch((error) => reject(error + ' Fail to update EventId'))

    })
};


module.exports.eventDisLikes = (userId, eventId) => {
    return new Promise((resolve, reject) => {
        Event.findOneAndUpdate({
            _id: eventId
        }, {
            '$pull': {
                like: userId
            }
        }, {
            'new': true
        }).then((result) => {
            console.log(result)
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'eventId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': false,
                    'message': 'Event dislike successfully.',
                    'data': result
                }
                resolve(responseData)
            }

        }).catch((error) => reject(error + ' Fail to update EventId'))

    })
};

// Route Search 
module.exports.filterEvent = (startDate, endDate, lat, lng, eventCategory1) => {
    return new Promise((resolve, reject) => {
        // console.log(startDate, lat, eventCategory1)
        if (parseInt(lng) > 0 && parseInt(lat) > 0 && startDate === null && eventCategory1.length === 0) {
            console.log('only lat & lng')
            Event.aggregate([{
                $geoNear: {

                    "near": {
                        type: "Point",
                        coordinates: [parseFloat(lng), parseFloat(lat)]
                    },

                    "maxDistance": 7 * 1000,
                    "distanceField": "distance",
                    "includeLocs": "dist.location",
                    "distanceMultiplier": 0.000621371,
                    "spherical": true
                }
            }]).then((result) => {
                if (result.length === 0) {
                    const responseData = {
                        'success': false,
                        'message': ' Not found.',
                        'data': result
                    }
                    resolve(responseData)
                } else {
                    const responseData = {
                        'success': true,
                        'message': 'Events Found Successfully.',
                        'data': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject(error))
        } else if (startDate === null && eventCategory1.length !== 0 && parseInt(lng) === 0 && parseInt(lat) === 0) {
                console.log('only category')
        var d = new Date(); // Today!
        d.setDate(d.getDate() - 1);
            Event.aggregate([{
                $match: {
                    $and: [{
                        startDate: {
                            $gte: new Date(d)
                        }
                    }, {
                        eventCategoryId: {
                            $in: eventCategory1
                        }
                    }]
                }

            }]).then((result) => {
                // console.log("Category=" + JSON.stringify(result))
                if (result == null) {
                    const responseData = {
                        'success': false,
                        'message': ' Not found.',
                        'data': result
                    }
                    resolve(responseData)
                } else {
                    const responseData = {
                        'success': true,
                        'message': 'Events Found Successfully.',
                        'data': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject(error))
        } else if (startDate !== '' && eventCategory1.length === 0 && parseInt(lng) === 0 && parseInt(lat) === 0) {
            console.log('only startDate')
            Event.aggregate([{
                $match: {

                    startDate: {
                        $gte: new Date(startDate)
                    }
                }

            }]).then((result) => {

                var dta = new Date(startDate);
                    const dt = result.filter((d) => d.startDate.getTime() === dta.getTime())
                 console.log("Date=" + dt /*JSON.stringify(result)*/)
                if (result == null) {
                    const responseData = {
                        'success': false,
                        'message': ' Not found.',
                        'data': result
                    }
                    resolve(responseData)
                } else {

                    const responseData = {
                        'success': true,
                        'message': 'Events Found Successfully.',
                        'data': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject(error))
        }else if ((startDate !== '' || eventCategory1.length !== 0) && parseInt(lng) === 0 && parseInt(lat) === 0) {
                console.log('both startDate & cat')     
             Event.aggregate([{
                $match: {
                    $and: [{
                        startDate: {
                            $gte: new Date(startDate)
                        }
                    }, {
                        eventCategoryId: {
                            $in: eventCategory1
                        }
                    }]
                }

            }]).then((result) => {
                  console.log(eventCategory1)  

                // console.log("Category=" + JSON.stringify(result))
                if (result == null) {
                    const responseData = {
                        'success': false,
                        'message': ' Not found.',
                        'data': result
                    }
                    resolve(responseData)
                } else {
                    const responseData = {
                        'success': true,
                        'message': 'Events Found Successfully.',
                        'data': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject(error))
        } else if(startDate === null && eventCategory1.length !== 0 && parseInt(lng) !== 0 && parseInt(lat) !== 0)  {
            console.log('both location & cate')
            Event.aggregate([{
                $geoNear: {

                    "near": {
                        type: "Point",
                        coordinates: [parseFloat(lng), parseFloat(lat)]
                    },

                    "maxDistance": 7 * 1000,
                    "distanceField": "distance",
                    "includeLocs": "dist.location",
                    "distanceMultiplier": 0.000621371,
                    "spherical": true
                }
            }]).then(async(result) => {
                const filterEventLocCat = []
               await result.map(async(ev) => {
                const arr1= eventCategory1

                const arr2 = ev.eventCategoryId

                let found = await arr2.some(r=> arr1.includes(r))
                console.log(found)
                if(found) {filterEventLocCat.push(ev) }
            })  
                const responseData = {
                        'success': true,
                        'message': 'Events Found Successfully.',
                        'data': filterEventLocCat
                    }

                resolve(responseData)
            }).catch((error) => reject('location & category not found'))
          
        }else if(startDate !== '' && eventCategory1.length === 0 && parseInt(lng) !== 0 && parseInt(lat) !== 0)  {
            console.log('both location & startDate')
            Event.aggregate([{
                $geoNear: {

                    "near": {
                        type: "Point",
                        coordinates: [parseFloat(lng), parseFloat(lat)]
                    },

                    "maxDistance": 7 * 1000,
                    "distanceField": "distance",
                    "includeLocs": "dist.location",
                    "distanceMultiplier": 0.000621371,
                    "spherical": true
                }
            }]).then((result) => {

                var dta = new Date(startDate);
                const v = result.filter(d => {
                console.log('d.startDate= ' + d.startDate)

                    return d.startDate  >= dta
                })
 
                const responseData = {
                        'success': true,
                        'message': 'Events Found Successfully.',
                        'data': v
                    }

                resolve(responseData)
            }).catch((error) => reject('location & category not found'))
          
        } else {
            console.log('ALL')
            Event.aggregate([{
                $geoNear: {

                    "near": {
                        type: "Point",
                        coordinates: [parseFloat(lng), parseFloat(lat)]
                    },

                    "maxDistance": 7 * 1000,
                    "distanceField": "distance",
                    "includeLocs": "dist.location",
                    "distanceMultiplier": 0.000621371,
                    "spherical": true
                }
            }]).then((result) => {

                var dta = new Date(startDate);
                const v = result.filter(d => d.startDate  >= dta)

                const filterEventLocCat = []
               v.map(async(ev) => {
                const arr1= eventCategory1

                const arr2 = ev.eventCategoryId

                let found = arr2.some(r=> arr1.includes(r))
               
                if(found) {filterEventLocCat.push(ev) }
            })  
                const responseData = {
                        'success': true,
                        'message': 'Events Found Successfully.',
                        'data': filterEventLocCat
                    }

                resolve(responseData)
            }).catch((error) => reject('location & category not found'))

        }

    })
};


module.exports.search = (check) => {
    return new Promise((resolve, reject) => {
        Event.find({
            $or: [{
                    city: {
                        $regex: new RegExp(check, 'i')
                    }
                },
                {
                    city: {
                        $regex: new RegExp(check, 'i')
                    }
                },
                {
                    address: {
                        $regex: new RegExp(check, 'i')
                    }
                },
                {
                    selectedCategoryName: {
                        '$in': [new RegExp(check, 'i')]
                    }
                }
            ]
        }).
        populate({
                path: 'eventCategory',
                select: 'categoryName'
            })
            .then((result) => {
                // console.log('result = ' + result)
                if (result.length === 0) {
                    console.log('result = ' + result)

                    const responseData = {
                        'success': false,
                        'message': 'Not found.',
                        'data': result
                    }
                    resolve(responseData)
                } else {
                    const responseData = {
                        'success': true,
                        'message': ' Event found successfully.',
                        'data': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject(error))
    })
};




/*return new Promise((resolve, reject) => {
  console.log(startDate, endDate, eventCategory)
  Event.find({ '$and' : [ {startDate : {'$gt': startDate} }, 
    {startDate : {'$lt': endDate} }], eventCategory:{ '$in':  eventCategory} })
       .then((result) => {
          console.log(result)
            if(result.length === 0) {
                const responseData = {
                  'success': false,
                  'message': 'Not found.',
                  'data': result

                }
                resolve(responseData)
            } else{
              const responseData = {
                'success': true, 
                'message': 'Draft data fetch successfully.', 
                'data': result
              }
              resolve(responseData)
          }
       }).catch((error) => reject(error))
    })
  };*/


module.exports.notification = (userId, eventId, notificationType) => {
    return new Promise((resolve, reject) => {
        Event.findOneAndUpdate({
                _id: eventId
            }, {
                '$push': {
                    notification: {
                        'user': userId,
                        'notificationType': notificationType
                    }
                }
            }, {
                'new': true
            })
            .then((result) => {
                console.log(result)
                if (result) {
                    const responseData = {
                        'success': true,
                        'message': 'Notification Create successfully.',
                        'data': result
                    }
                    resolve(responseData)
                } else {


                    const responseData = {
                        'success': false,
                        'message': 'eventId not found.',
                        'data': result
                    }
                    resolve(responseData)


                }
            }).catch((error) => reject(error))
    })
};



module.exports.fetchAllNotification = (userId) => {
    return new Promise((resolve, reject) => {
        Event.find({
            userId: userId
        }).populate("notification.user").sort({
            lastModifiedDate: -1
        }).then(async (result) => {
          
            if (result.length === 0) {} else {
                const arr = [];
                const notificationResult = result.map((event, index) => {
                    event.notification.map((n) => {

                        arr.push({
                            'eventId': event._id,
                            'eventName': event.title,
                            'notificationType': n.notificationType,
                            '_id': n._id,
                            'user': n.user
                        });
                    })
                })
                const responseData = {
                    'success': true,
                    'message': ' fetch All Notification List successfully.',
                    'data': arr
                }
                resolve(responseData)
            }
        }).catch((error) => reject(error))
    })
};


module.exports.interestSave = (userId, eventId) => {
    return new Promise((resolve, reject) => {
        Event.findOneAndUpdate({
            _id: eventId
        }, {
            '$addToSet': {
                interest: userId
            }
        }, {
            'new': true
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'eventId not found.',
                }
                resolve(responseData)
            } else {

                const responseData = {
                    'success': true,
                    'message': 'Event Save successfully.',
                    'data': result
                }
                resolve(responseData)
            }

        }).catch((error) => reject(error + ' Fail to update EventId'))

    })
};

module.exports.removeInterestSave = (userId, eventId) => {
    return new Promise((resolve, reject) => {
        Event.findOneAndUpdate({
            _id: eventId
        }, {
            '$pull': {
                interest: userId
            }
        }, {
            'new': true
        }).then((result) => {
            console.log(result)
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'eventId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': false,
                    'message': 'Event Remove successfully.',
                    'data': result
                }
                resolve(responseData)
            }

        }).catch((error) => reject(error + ' Fail to update EventId'))

    })
};

module.exports.fetchInterest = (userId) => {
    console.log("userId=" + userId)
    return new Promise((resolve, reject) => {
        Event.find({
            interest: {
                $in: [userId]
            }
        }).sort({
            _id: -1
        }).populate('userId').then(async (result) => {
            // console.log(result)
            if (result.length === 0) {
                const responseData = {
                    'success': false,
                    'message': 'userId 1not found.',
                    'data': result
                }
                resolve(responseData)
            } else {
                let arr = [];

                 let userData = await User.findOne({  _id: userId  })
 
                const eventResult = result.map((event) => {
                    const found = event.interest.filter(id => id.toString() === userId);
                    if (found.length !== 0) {
                        arr.push({
                            ...event.toObject(),
                            userInfo: userData
                        })
                    }
                })
                // console.log(arr)
                const responseData = {
                    'success': true,
                    'message': 'All save Events fetch successfully.',
                    'data': arr
                }
                resolve(responseData)
            }
        }).catch((error) => reject('eventId not found'))
    })
};