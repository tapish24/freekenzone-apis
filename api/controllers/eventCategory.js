const mongoose = require("mongoose");
const EventCategory = require("../models/eventCategory");



module.exports.addEventCategory = (addEventcategory) => {
    return new Promise((resolve, reject) => {
        const newEventCategory = new EventCategory(addEventcategory).save().
        then(eventCategoryInfo => {
            const responseData = {
                'success': true,
                'message': 'Successful created EventCategory.',
                'data': eventCategoryInfo
            }
            resolve(responseData)

        }).catch((error) => reject(error))

    });
};

module.exports.viewEventCategory = (eventCategoryId) => {
    return new Promise((resolve, reject) => {
        EventCategory.findOne({
            _id: eventCategoryId
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    success: false,
                    msg: 'eventCategoryId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'eventCategory data fetch successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('eventId not found'))
    })
};

module.exports.categoryUpdate = (eventCategoryId, brifData) => {
    return new Promise((resolve, reject) => {
        EventCategory.findOneAndUpdate({
            _id: eventCategoryId
        }, {
            '$set': brifData
        }, {
            'new': true
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'eventCategoryId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'EventCategory Update successfully.',
                    'data': result,
                }
                resolve(responseData)
            }

        }).catch((error) => reject(error + ' Fail to update EventId'))

    })
};

module.exports.categoryStatusUpdate = (eventCategoryId, brifData) => {
    return new Promise((resolve, reject) => {
        EventCategory.findOneAndUpdate({
            _id: eventCategoryId
        }, {
            '$set': brifData
        }, {
            'new': true
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'eventCategoryId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'EventCategory Status Update successfully.',
                    'data': result,
                }
                resolve(responseData)
            }

        }).catch((error) => reject(error + ' Fail to update EventId'))

    })
};


module.exports.EventCategoryDelete = (eventcategoryId) => {
    return new Promise((resolve, reject) => {
        EventCategory.findOneAndDelete({
            _id: eventcategoryId
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'eventCategoryId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': ' EventCategory deleted Successfully.'
                    //user: result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('Failed to find EventCategoryId'))
    })
};


module.exports.viewallCategory = (eventCategoryId) => {
    return new Promise((resolve, reject) => {
        EventCategory.find({}).sort({
            _id: -1
        }).then((result) => {
            if (result == null) {} else {
                const responseData = {
                    'success': true,
                    'message': ' fetch All EventCategory List successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('eventCategoryId not found'))
    })
};