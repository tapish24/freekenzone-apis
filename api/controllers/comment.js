const mongoose = require("mongoose");
const Event = require("../models/event");


module.exports.createComment = (comment, event) => {
    return new Promise((resolve, reject) => {
        Event.findOneAndUpdate({
                _id: event
            }, {
                '$push': {
                    comment: comment
                }
            }, {
                'new': true
            })
            .then((comment) => {
                const responseData = {
                    'success': true,
                    'message': 'Successful created new Comment.',
                    'data': comment
                }
                resolve(responseData)
            }).catch((error) => reject(error))
    });
};



module.exports.fetchSingleComment = (eventId, userId) => {
    return new Promise((resolve, reject) => {
        Event.findOne({
            _id: eventId,
            'comment.user': userId
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'comment not found.',
                }
                resolve(responseData)
            } else {
                const comment = result.comment.filter(id => id.user.toString() === userId);

                const responseData = {
                    'success': true,
                    'message': 'comment data fetch successfully.',
                    'data': comment
                }
                resolve(responseData)
            }
        }).catch((error) => reject('commentId not found'))
    })
};


module.exports.fetchAllComment = (eventId) => {
    return new Promise((resolve, reject) => {
        Event.findOne({
            _id: eventId
        }).populate('comment.user').then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'comment not found.',
                }
                resolve(responseData)
            } else {
                const comment = result.comment.map((commentInfo) => {
                    return commentInfo
                });

                const responseData = {
                    'success': true,
                    'message': 'comment data fetch successfully.',
                    'data': comment.reverse()
                }
                resolve(responseData)
            }
        }).catch((error) => reject('commentId not found'))
    })
};



module.exports.CommentUpdate = (commentId, brifData) => {
    return new Promise((resolve, reject) => {
        Comment.findOneAndUpdate({
            _id: commentId
        }, {
            '$set': brifData
        }, {
            'new': true
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'commentId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'Comment Update successfully.',
                    'data': result,
                }
                resolve(responseData)
            }

        }).catch((error) => reject(error + ' Fail to update commentId'))

    })
};


module.exports.CommentDelete = (eventId, commentId) => {
    return new Promise((resolve, reject) => {
        Event.update({
                _id: eventId
            }, {
                '$pull': {
                    'comment': {
                        _id: commentId
                    }
                }
            })
            .then((result) => {

                if (result == null) {
                    const responseData = {
                        'success': false,
                        'message': 'CommentId not found.',
                    }
                    resolve(responseData)
                } else {
                    const responseData = {
                        'success': true,
                        'message': ' Comment deleted Successfully.',
                        'user': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject('Failed to find userId'))
    })
};

module.exports.viewCommentsList = (commentId) => {
    return new Promise((resolve, reject) => {
        Comment.findOne({
            _id: userId
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'userId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'comment List data fetch successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('commentId not found'))
    })
};