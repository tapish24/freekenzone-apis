const mongoose = require("mongoose");
const Like = require("../models/like");



module.exports.eventLike = (userId,eventId) => {
    return new Promise((resolve, reject) => {
        Like.findOne({ userId: userId },{ eventId: eventId }
          ).then((result) => {
            if(result === null) {
             const newLike = new Like({
                userId:userId,
                eventId: eventId
              })
             newLike.save().then((likeInfo) => {
                        const responseData = {
                            'message': "Instagram login successful",
                            'success': true,
                            'data': likeInfo
                        }
                        resolve(responseData)
              }).catch((error) => reject(error))
            }else{
                        const responseData = {
                            'message': "Instagram login successful",
                            'success': true,
                             'data': result
                        }
                        resolve(responseData)
            } 

        }).catch((error) => reject(error))
           
    })
};
