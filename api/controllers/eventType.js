const mongoose = require("mongoose");
const EventType = require("../models/eventType");



module.exports.createEventType = (addEventtype) => {
    return new Promise((resolve, reject) => {
        const newEventType = new EventType(addEventtype).save().then(eventTypeInfo => {
            const responseData = {
                'success': true,
                'message': 'Successful created EventType.',
                'eventTypeId': eventTypeInfo._id
            }
            resolve(responseData)

        }).catch((error) => reject('Fail user registeration'))

    });
};

module.exports.viewEventType = (eventTypeId) => {
    return new Promise((resolve, reject) => {
        EventType.findOne({
            _id: eventTypeId
        }).then((result) => {
            if (result === null) {
                const responseData = {
                    'success': false,
                    'message': 'eventTypeId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'eventType data fetch successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('eventId not found'))
    })
};
module.exports.EventTypeUpdate = (eventTypeId, brifData) => {
    return new Promise((resolve, reject) => {
        EventType.findOneAndUpdate({
            _id: eventTypeId
        }, {
            '$set': brifData
        }, {
            'new': true
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'eventTypeId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'EventType Update successfully.',
                    'data': result,
                }
                resolve(responseData)
            }

        }).catch((error) => reject(error + ' Fail to update EventId'))

    })
};


module.exports.EventTypeDelete = (eventTypeId) => {
    return new Promise((resolve, reject) => {
        EventType.findOneAndDelete({
            _id: eventTypeId
        }).then((result) => {
            if (result === null) {
                const responseData = {
                    'success': false,
                    'message': 'EventTypeId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': ' EventType deleted Successfully.'
                    //user: result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('Failed to find userId'))
    })
};

module.exports.viewallEventType = (evenTypeId) => {
    return new Promise((resolve, reject) => {
        EventType.find({}).then((result) => {
            if (result.length === 0) {
                resolve([]);
            } else {
                const responseData = {
                    'success': true,
                    'message': ' fetch All EventType List successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('eventTypeId not found'))
    })
};