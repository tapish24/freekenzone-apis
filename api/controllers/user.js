const mongoose = require("mongoose");
const bcrypt = require("bcrypt-nodejs");
const jwt = require("jsonwebtoken");
const nodemailer = require('nodemailer');
const generator = require('generate-password');
const passwordHash = require('password-hash');
const User = require("../models/user");
const SettingSave = require("../models/settingSave");

var gcm = require('node-gcm');




module.exports.settingSaveFun = (preferanceCategory,preferanceLocation,preferanceAddress,userId) => {
    console.log(preferanceCategory)
    console.log(preferanceLocation)
    console.log(preferanceAddress)
    console.log(userId)

    return new Promise((resolve, reject) => {
        User.findOneAndUpdate({
                _id: userId
            }, {
                '$push': {
                    saveSetting: {
                        'preferanceCategory': preferanceCategory,
                        'preferanceLocation': preferanceLocation,
                        'preferanceAddress' : preferanceAddress
                    }
                },
                
            }, {
                'new': true
            }).then((result) =>{
             console.log(result)
      const responseData = {
        success: true,
        msg : 'Successfully created setting.',
        data:result 
      }
        resolve(responseData)
    }).catch((error) => reject('Requested resource not found'))
  });
}



module.exports.viewSaveSettingFun = (settingId) => {
    return new Promise((resolve, reject) => {
        SettingSave.findOne({
            _id: settingId
        }).then((result) => {
            if (result === null) {} else {
                const responseData = {
                    'success': true,
                    'message': ' fetch All Setting List successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('userId not found'))
    })
};

exports.userSignup = (req, res, next) => {
    User.findOne({
            email: req.body.email
        })
        .then(user => {
            if (user) {
                return res.status(200).json({
                    message: "Email already exists",
                    "Register_status": false
                });
            } else {

                const user = new User({
                    _id: new mongoose.Types.ObjectId(),
                    email: req.body.email,
                    image: req.body.image,
                    firstName: req.body.firstName,
                    lastName: req.body.lastName,
                    dateOfBirth: req.body.dateOfBirth,
                    gender: req.body.gender,
                    followers: req.body.followers,
                    following: req.body.following,
                    deviceType: req.body.deviceType,
                    deviceId: req.body.deviceId,
                    password: req.body.password,
                    role: req.body.role || 'user'

                });
                user
                    .save()
                    .then(result => {
                        res.status(201).json({
                            'message': "User created successfully",
                            'Register_status': true,
                            'userId': user.id
                        });
                    })
                    .catch(err => {
                        res.status(500).json({
                            error: err
                        });
                    });

            }
        });
};



module.exports.userLogin = (email, pwd, role, deviceType, deviceId) => {
    return new Promise((resolve, reject) => {
        User.findOne({
            email: email
        }).then((result) => {
            console.log(result)
            if (result === null) {
                const responseData = {
                    success: false,
                    msg: 'Please pass correct Email',
                }
                resolve(responseData)

            } else {
                result.comparePassword(pwd, result.password, (err, isMatch) => {
                    if (err) {
                        const responseData = {
                            success: false,
                            msg: 'Something is wrong',
                        }
                        resolve(responseData)
                    }
                    if (isMatch) {

                        User.findOneAndUpdate({
                            _id: result._id
                        }, {

                            '$set': {

                                'deviceType': deviceType,
                                'deviceId': deviceId
                            }
                        }, {
                            'new': true
                        }).then((N) => {
                            console.log("N=" + N)
                        }).catch((error) => reject(error))


                        const token = jwt.sign({
                                _id: result._id,
                                email: result.email,
                                username: result.username,
                                role: result.role,
                            },
                            'secret', {
                                expiresIn: "365d"
                            }
                        );
                        const responseData = {
                            'message': "login successful",
                            'success': true,
                            token: token,
                            'data': result

                        }
                        resolve(responseData)
                    } else {
                        const responseData = {
                            message: "password incorrect",
                            'success': false,
                        }
                        resolve(responseData)
                    }
                })
            }
        }).catch((error) => {
            reject('Requested resource not found')
        })
    })
};




module.exports.ProfileUpdate = (userId, brifData) => {
    return new Promise((resolve, reject) => {
        User.findOneAndUpdate({
            _id: userId
        }, {
            '$set': brifData
        }, {
            'new': true
        }).then((result) => {
            if (result == null) {
                const responseData = {
                    'success': false,
                    'message': 'userId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': 'User Update successfully.',
                    'data': result,
                }
                resolve(responseData)
            }
        }).catch((error) => reject(error + ' Fail to update profileId'))
    })
};


// module.exports.setttingUpdateFunc = (userId, brifData) => {
//     return new Promise((resolve, reject) => {
//         User.findOneAndUpdate({
//             _id: userId
//         }, {
//             '$set': brifData
//         }, {
//             'new': true
//         }).then((result) => {
//             if (result == null) {
//                 const responseData = {
//                     'success': false,
//                     'message': 'userId not found.',
//                 }
//                 resolve(responseData)
//             } else {
//                 const responseData = {
//                     'success': true,
//                     'message': 'setting save successfully.',
//                     'data': result,
//                 }
//                 resolve(responseData)
//             }
//         }).catch((error) => reject(error + ' Fail to update profileId'))
//     })
// };


module.exports.Fblogin = (email, firstName, facebookId) => {
    return new Promise((resolve, reject) => {
        User.findOne({
            email: email
        }).then((result) => {
            if (result === null) {
                const newUser = new User({
                    firstName: firstName,
                    email: email,
                    facebookId: facebookId
                })
                newUser.save().then((userInfo) => {
                    const token = jwt.sign({
                            _id: userInfo._id,
                            facebookId: userInfo.facebookId,
                        },
                        'secret', {
                            expiresIn: "365d"
                        }
                    );
                    const responseData = {
                        'message': "Facebook login successful",
                        'success': true,
                        token: token,
                        'data': userInfo

                    }
                    resolve(responseData)
                }).catch((error) => reject(error))

            } else {
                const token = jwt.sign({
                        _id: result._id,
                        facebookId: result.facebookId,

                    },
                    'secret', {
                        expiresIn: "365d"
                    }
                );
                const responseData = {
                    'message': "Facebook login successful",
                    'success': true,
                    token: token,
                    'data': result

                }
                resolve(responseData)
            }

        }).catch((error) => reject(error))

    })
};


module.exports.instaLogin = (firstName, instagramId) => {
    return new Promise((resolve, reject) => {
        User.findOne({
            instagramId: instagramId
        }).then((result) => {
            if (result === null) {
                const newUser = new User({
                    firstName: firstName,
                    instagramId: instagramId
                })
                newUser.save().then((userInfo) => {
                    const token = jwt.sign({
                            _id: userInfo._id,
                            instagramId: userInfo.instagramId,

                        },
                        'secret', {
                            expiresIn: "365d"
                        }
                    );
                    const responseData = {
                        'message': "Instagram login successful",
                        'success': true,
                        token: token,
                        'data': userInfo

                    }
                    resolve(responseData)
                }).catch((error) => reject(error))

            } else {
                const token = jwt.sign({
                        _id: result._id,
                        instagramId: result.instagramId,
                    },
                    'secret', {
                        expiresIn: "365d"
                    }
                );
                const responseData = {
                    'message': "Instagram login successful",
                    'success': true,
                    token: token,
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject(error))

    })
};


module.exports.viewallUser = (userId) => {
    return new Promise((resolve, reject) => {
        User.find({
            role: 'user'
        }).sort({
            _id: -1
        }).then((result) => {
            if (result === null) {} else {
                const responseData = {
                    'success': true,
                    'message': ' fetch All User List successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('userId not found'))
    })
};

module.exports.testEmail = (email) => {
    return new Promise((resolve, reject) => {
        User.findOne({
                email: email
            })
            .then(user => {
                if (user) {

                    var password = generator.generate({
                        length: 10,
                        numbers: true
                    });
                    bcrypt.genSalt(10, function(err, salt) {
                        if (err) {
                            return next(err);
                        }
                        bcrypt.hash(password, salt, null, function(err, hash) {
                            if (err) {
                                return next(err);
                            }
                            password = hash;
                            User.findOneAndUpdate({
                                _id: user._id
                            }, {
                                '$set': {
                                    password: password
                                }
                            }, {
                                'new': true
                            }).then((dara) => {}).catch((error) => console.log('error = ' + error))
                        });
                    });
                    var transporter = nodemailer.createTransport({
                        service: 'gmail',
                        auth: {
                            user: 'phpbatch34@gmail.com',
                            pass: '123@@123'
                        }
                    });

                    var mailOptions = {
                        from: 'youremail@gmail.com',
                        to: email,
                        subject: 'Forgot Password  ',
                        text: 'That is the New Password! =   ' + password,
                        // Password: password
                    };

                    transporter.sendMail(mailOptions, function(error, info) {
                        if (error) {
                            console.log(error);
                        } else {
                            const responseData = {
                                'success': true,
                                'message': ' Email send successfully .',
                                'data': info
                            }
                            resolve(responseData)
                            // .catch((error) => reject('userId not found'))

                            // console.log('Email sent: ' + info.response);
                            // res.send(info)
                        }
                    });
                } else {
                    const responseData = {
                        success: false,
                        msg: 'EmailId not found',
                    }
                    resolve(responseData)
                }
            });
    });
};

module.exports.otherUser = (userId, loginUser) => {
    console.log("loginUser=" + loginUser)
    return new Promise((resolve, reject) => {
        User.findOne({
            _id: userId
        }).then((user) => {
            // console.log(user)

            if (user == null) {
                const responseData = {
                    success: false,
                    msg: 'userId not found.',
                }
                resolve(responseData)
            } else {
                User.findOne({
                    _id: loginUser
                }).then((result) => {

                    let isFollowing
                    const userFound = result.following.filter(id => {
                        return id.user.toString() == userId
                    });
                    console.log('userFound = ' + userFound)
                    // console.log("userFound=" + userFound[0].isPending, typeof userFound[0].isPending)

                    if (userFound.length > 0) {
                        if (userFound[0].isPending === true) {
                            isFollowing = 2

                        } else {

                            isFollowing = 1
                        }

                    } else {
                        isFollowing = 0
                    }
                    console.log('isFollowing = ' + isFollowing)


                    resolve({
                        'success': true,
                        'message': 'User data fetch successfully.',
                        'data': {
                            "image": user.image,
                            "firstName": user.firstName,
                            "lastName": user.lastName,
                            "dateOfBirth": user.dateOfBirth,
                            "gender": user.gender,
                            "companyName": user.companyName,
                            "phoneNumber": user.phoneNumber,
                            "skypeId":user.skypeId,
                            "instagramId":user.instagramId,
                            "state": user.state,
                            "city": user.city,
                            "address": user.address,
                            "pinCode": user.pinCode,
                            "facebookId": user.facebookId,
                            "role": user.role,
                            "_id": user._id,
                            "email": user.email,
                            "followers": user.followers,
                            "following": user.following,
                            "isFollowing": isFollowing,
                            "notificationStatus": user.notificationStatus,
                            "notification": user.notification,
                            "password": user.password,
                            'followingCount': user.following.length,
                            'followerCount': user.followers.length,

                        }
                    })

                })

                // resolve(user)
            }
        }).catch((error) => reject('userId not found'))
    })
};

module.exports.viewProfileFunc = (userId) => {
    return new Promise((resolve, reject) => {
        console.log('viewProfileFunc')
        User.findOne({
            _id: userId
        }).then((user) => {

            if (user == null) {
                const responseData = {
                    success: false,
                    msg: 'userId not found.',
                }
                resolve(responseData)
            } else {
                resolve({
                    'success': true,
                    'message': 'User data fetch successfully.',
                    'data': {
                        "image": user.image,
                        "firstName": user.firstName,
                        "lastName": user.lastName,
                        "dateOfBirth": user.dateOfBirth,
                        "gender": user.gender,
                        "companyName": user.companyName,
                        "phoneNumber": user.phoneNumber,
                        "state": user.state,
                        "city": user.city,
                        "address": user.address,
                        "pinCode": user.pinCode,
                        "facebookId": user.facebookId,
                        "role": user.role,
                        // "isFollow":isFollow,
                        "_id": user._id,
                        "email": user.email,
                        "followers": user.followers,
                        "following": user.following,
                        "skypeId":user.skypeId,
                        "instagramId":user.instagramId,
                        // "isFollowing": user.isFollowing,
                        "notificationStatus": user.notificationStatus,
                        "notification": user.notification,
                        "password": user.password,
                        'followingCount': user.following.length,
                        'followerCount': user.followers.length,
                    }
                })
                // resolve(user)
            }
        }).catch((error) => reject('userId not found'))
    })
};


module.exports.userFollow = (following1, userId, notificationStatus, deviceId) => {
    console.log("recieverUserId=" +JSON.stringify(following1) )
    // console.log("loginUserId=" + JSON.stringify(following1))
    // console.log("UserId=" + userId)
    return new Promise((resolve, reject) => {
        User.findOneAndUpdate({
                _id: userId
            }, {
                '$push': {
                    'following': following1,

                }
            }, {
                'new': true
            })
            .then(async (result) => {
                if (result === null) {
                    const responseData = {
                        'success': false,
                        'message': 'userId not found.',
                    }

                    resolve(responseData)
                } else {
                   await User.findOneAndUpdate({
                        _id: following1.user
                    }, {
                        '$push': {
                            // 'following': following1,
                            'notification': {
                                'requestedUserId': userId,
                                'body': result.firstName  + ' Wants to Follow You ',
                                'notificationStatus':1,
                                'acceptUserId': following1.user

                            },

                        },
                       
                    }, {
                        'new': true
                    }).then((N) => {
                         console.log("N=" + N)

                        const gcmKey = '1061332147717'; // Your gcm key in quotes
                        const deviceToken = N.deviceId; // Receiver device token
                        const sender = new gcm.Sender('AAAA9xxTXgU:APA91bG62-K1FmioZuDd9N-85h9bMBDziLSoXediC6iIsA_9kIL3y_acL-eHkRJOrhbc8k0l0IQOLdtWT5JnB8YRa8djB8yqs6wyp2CxPUw5ss8teFuRcDIN-Njs5rWqzL0dtM6uq2aW');

                        var message = new gcm.Message();

                        message.addData({
                            title: 'Follow Request',
                            body: result.firstName  + ' Wants to Follow You ',
                            image: "http://jokingfriend.com/BeehapImageUpload/img/154772445544262.jpg",
                            loginUserId: userId,
                            otherProperty: true,
                        });
                       /*   message.save().then().
                         catch((error) => reject(error))*/
                        // console.log("M=" + M)
                          
                        sender.send(message, {
                            registrationIds: [deviceToken]
                        }, (err) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log('Sent');
                            }
                        });


                        let isFollowing
                        const userFound = result.following.filter(id => {
                            return id.user.toString() == userId
                        });

                        if (userFound.length > 0) {
                            if (userFound[0].isPending === true) {
                                isFollowing = 2

                            } else {

                                isFollowing = 1
                            }

                        } else {
                            isFollowing = 0
                        }


                        resolve({
                            'success': true,
                            'message': 'User follow successfully.',
                            'data': {
                                "image": result.image,
                                "firstName": result.firstName,
                                "lastName": result.lastName,
                                "dateOfBirth": result.dateOfBirth,
                                "gender": result.gender,
                                "companyName": result.companyName,
                                "phoneNumber": result.phoneNumber,
                                "state": result.state,
                                "city": result.city,
                                "address": result.address,
                                "pinCode": result.pinCode,
                                "facebookId": result.facebookId,
                                "role": result.role,
                                "_id": result._id,
                                "email": result.email,
                                "followers": result.followers,
                                "following": result.following,
                                "isFollowing": isFollowing,
                                "notificationStatus": result.notificationStatus,
                                "password": result.password,
                                'followingCount': result.following.length,
                                'followerCount': result.followers.length,

                            }
                        })



                    }).catch((error) => reject(error + ' Fail to update EventId'))

                };
            });
    });
};

module.exports.requestAccept = (acceptUserId, requestedUserId, notificationStatus, isPendingStatus) => {
    return new Promise((resolve, reject) => {
        User.findOneAndUpdate({
                _id: acceptUserId
            }, {
                '$push': {
                    followers: {
                        'user': requestedUserId,
                        'isPending': isPendingStatus
                    }
                },
                
            }, {
                'new': true
            })
            .then(async (result) => {
                // console.log(result)
                if (result === null) {
                    const responseData = {
                        'success': false,
                        'message': 'userId not found.',
                        'data': result
                    }
                    resolve(responseData)
                } else {

                    await User.findOneAndUpdate({
                        'following.user': acceptUserId
                    }, {

                        '$set': {

                            'following.$.isPending': false
                        }
                    }, {
                        'new': true
                    }).then().
                    catch((error) => reject(error))


                  await User.findOneAndUpdate({
                            '_id': acceptUserId,
                            'notification.requestedUserId': requestedUserId
                        }, {
                            '$pull': {
                                'notification': {
                                    'requestedUserId': requestedUserId
                                }
                            },
                            '$set': {

                                'notificationStatus': 0
                            }
                        }, {
                            'new': true
                        }).then(async(T) => {
                            // console.log(T)
                      /*  .then(t => console.log(JSON.stringify(t))).
                    catch((error) => reject(error))
*/

                    await User.findOneAndUpdate({
                        _id: requestedUserId
                    }, {
                        '$push': {
                            // 'following': following1,
                            'notification': {
                                'requestedUserId': acceptUserId,
                                'body': result.firstName  + ' Accept your Request ',
                            },

                        }
                    }, {
                        'new': true
                    }).then((N) => {
                          // console.log("N=" + N)
                          const gcmKey = '1061332147717'; // Your gcm key in quotes
                        const deviceToken = N.deviceId; // Receiver device token
                        const sender = new gcm.Sender('AAAA9xxTXgU:APA91bG62-K1FmioZuDd9N-85h9bMBDziLSoXediC6iIsA_9kIL3y_acL-eHkRJOrhbc8k0l0IQOLdtWT5JnB8YRa8djB8yqs6wyp2CxPUw5ss8teFuRcDIN-Njs5rWqzL0dtM6uq2aW');

                        var message = new gcm.Message();

                        message.addData({
                            title: 'Follow Request',
                            body: result.firstName  + ' Accept your Request ',
                            image: "http://jokingfriend.com/BeehapImageUpload/img/154772445544262.jpg",
                            requestedUserId: requestedUserId,
                            otherProperty: true,
                        });
                           console.log("message="+ JSON.stringify(message.params.data.body))
                        sender.send(message, {
                            registrationIds: [deviceToken]
                        }, (err) => {
                            if (err) {
                                console.error(err);
                            } else {
                                console.log('Sent');
                            }
                        });

                        
                        // result.save().then((a) => {
                            const responseData = {
                                'success': true,
                                'message': 'Request Accept successfully.',
                                'data': T,
                                // 'count': a.followers.length
                            }
                            resolve(responseData)
                        })

                    }).catch((error) => reject(error + ' Fail to update EventId'))

                };
            });
    });
};

module.exports.requestReject = (rejectUserId, requestedUserId) => {
    return new Promise((resolve, reject) => {
        console.log(rejectUserId)
        User.findOneAndUpdate({
                '_id': requestedUserId,
                'following.user': rejectUserId
            }, {
                '$pull': {
                    'following': {
                        'user': rejectUserId
                    }
                }
            }, {
                'new': true
            })
            .then(async (result) => {
                if (result === null) {
                    const responseData = {
                        'success': false,
                        'message': 'userId not found.',
                        'data': result
                    }
                    resolve(responseData)
                } else {
                    await User.findOneAndUpdate({
                            '_id': rejectUserId,
                            'notification.requestedUserId': requestedUserId
                        }, {
                            '$pull': {
                                'notification': {
                                    'requestedUserId': requestedUserId
                                }
                            },
                            '$set': {

                                'notificationStatus': 0
                            }
                        }, {
                            'new': true
                        })
                        .then(t => console.log(JSON.stringify(t))).
                    catch((error) => reject(error))

                    // result.isFollowing = 0
                    // result.save().then((isFol) =>{
                    const responseData = {
                        'success': true,
                        'message': 'Request reject successfully.',
                        'data': result
                    }
                    resolve(responseData)


                    // resolve(responseData)
                    // })                


                };
            }).catch((error) => reject(error));
    });
};


module.exports.userUnFollow = (requestedtUserId, acceptUserId) => {
    return new Promise((resolve, reject) => {

        User.findOneAndUpdate({
                    '_id': requestedtUserId,
                    'following.user': acceptUserId
                }, {
                    '$pull': {
                        'following': {
                            'user': acceptUserId
                        },
                    }
                }, {
                    'new': true
                })
            .then((result) => {
                if (result === null) {
                    const responseData = {
                        'success': false,
                        'message': 'userId not found.',
                        'data': result
                    }
                    resolve(responseData)
                } else {
                    User.findOneAndUpdate({
                                '_id': acceptUserId,
                                'followers.user': requestedtUserId
                            }, {
                                '$pull': {
                                    'followers': {
                                        'user': requestedtUserId
                                    },
                                }
                            },  {
                                'new': true
                            })
                        .then().catch((error) => reject(error))
                    const responseData = {
                        'success': true,
                        'message': 'User Unfollow Successfully.',
                        'data': result
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject(error + ' Fail to update EventId'))
    })
};

module.exports.contactVisibility = (user, requestedUserId) => {
    return new Promise((resolve, reject) => {
        User.findOneAndUpdate({
                '_id': user,
                'followers.user': requestedUserId
            }, {
                '$set': {
                    'followers.$.isContactVisible': true,
                }
            }, {
                'new': true
            })
            .then((result) => {
                if (result === null) {
                    const responseData = {
                        'success': false,
                        'message': 'userId not found.',
                        'data': []
                    }
                    reject(responseData)
                } else {
                    /*    User.findOneAndUpdate({
                                'following.user': acceptUserId
                            }, {
                                '$set': {
                                    'following.$.isPending': isPendingStatus
                                }
                            }, {
                                'new': true
                            })
                            .then(() => {*/
                    const responseData = {
                        'success': true,
                        'message': 'Request accepted successfully.',
                        'data': result /* {'followersCount': result}*/
                    }
                    resolve(responseData)
                }
            }).catch((error) => reject(error + ' Fail to update EventId'))
    })
};




module.exports.notification = (userId) => {
    return new Promise((resolve, reject) => {
        User.findOne({
            _id: userId
        }).then((result) => {
            if (result.length === 0) {
                const responseData = {
                    success: false,
                    msg: 'userId not found.',
                }
                resolve(responseData)
            } else {
                const responseData = {
                    'success': true,
                    'message': ' fetch All Notification successfully.',
                    'data': result.notification.reverse(),
                    // 'notificationStatus':result.notificationStatus
                }
                resolve(responseData)
            }
        }).catch((error) => reject('userId not found'))
    })
};


module.exports.notificationList = (userId) => {
    return new Promise((resolve, reject) => {
        User.findOne({
           _id: userId
        }).then(async(result) => {
          
            if (result.length === 0) {
               const responseData = {
                    success: false,
                    msg: 'userId not found.',
                }
                resolve(responseData)
            } else {
                result.notification.map(async(r) => {          
                    r.isSeen = true;
                        return r;    

                });

              await result.save().then((y ) => {
            const responseData = {
                                'success': true,
                                'message': ' fetch All Notification successfully.',
                                'data': y.notification.reverse(),
                            }
                resolve(responseData)
              }).catch((err) => reject(err));
                
            }
        }).catch((error) => reject('userId not found'))
    })
};


module.exports.followingList = (userId) => {
    return new Promise((resolve, reject) => {
        User.find({
           _id: userId
        }).sort({
            _id: -1
        }).populate('following.user').then((result) => {
            console.log(result)
            if (result.length === 0) {
                reject('Following not found')
            } else {
                const responseData = {
                    'success': true,
                    'message': ' fetch All Following Users successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('userId not found'))
    })
};


module.exports.followersList = (userId) => {
    return new Promise((resolve, reject) => {
        User.find({
           _id: userId
        }).sort({
            _id: -1
        }).populate('followers.user').then((result) => {
            console.log(result)
            if (result.length === 0) {
                reject('Following not found')
            } else {
                const responseData = {
                    'success': true,
                    'message': ' fetch All Followers Users successfully.',
                    'data': result
                }
                resolve(responseData)
            }
        }).catch((error) => reject('userId not found'))
    })
};