
module.exports.filterObject = (subject, keys) => {
	const matchingKeys = Object.keys(subject).filter(k => keys.indexOf(k) !== -1)
	const matchingObject = {}
	matchingKeys.map(mk => matchingObject[mk] = subject[mk])

	return matchingObject
}